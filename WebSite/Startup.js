
angular.module('SimpleCsv', []);

angular
  .module('Application', [
    'ngRoute',
    'SimpleCsv',
    'ui.bootstrap',
    'ui.notify'
  ])
  .config(function ($routeProvider, $httpProvider, $provide) {
         
      $routeProvider
        .when('/', {
          templateUrl: 'Components/Home.html',
          controller: 'HomeController'
        })
        .when('/UpdatePerson', {
          templateUrl: 'Components/UpdatePerson.html',
          controller: 'UpdatePersonController'
        })
        .when('/UploadPersonCsv', {
          templateUrl: 'Components/UploadPersonCsv.html',
          controller: 'UploadPersonCsvController'
        })
        .when('/PersonView', {
          templateUrl: 'Components/PersonView.html',
          controller: 'PersonViewController'
        })
        .otherwise({
          redirectTo: '/'
        });
      
       $provide.factory('httpInterceptor', function ($q, $window, $rootScope, $location) {
           
                return {
                    // On request success
                    request: function (config) {

                        return config || $q.when(config);
                    },

                    // On request failure
                    requestError: function (rejection) {
                        
                        // Return the promise rejection to be consume downstream
                        return $q.reject(rejection);
                    },

                    // On response success
                    response: function (response) {
                      
                        return response || $q.when(response);
                    },

                    // On response failure
                    responseError: function (rejection) {
                     
                        return $q.reject(rejection);
                    }
                };
            });

            $httpProvider.interceptors.push('httpInterceptor');
    
  })
.run(function ($location, $window, $rootScope) {

});
