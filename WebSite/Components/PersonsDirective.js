angular.module('SimpleCsv')
	.directive('persons', function(){
        return {
             restrict: 'E',
                priority: 0,
                templateUrl: 'Components/Persons.html',
                controller: 'PersonsController'
            }
        });