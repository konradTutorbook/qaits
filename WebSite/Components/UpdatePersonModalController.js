angular.module('SimpleCsv')
	.controller('UpdatePersonModalController', function($scope, $rootScope, $modalInstance, person){

		$scope.Person = person;

		$scope.ok = function () {
		   $modalInstance.close();
		};

		$scope.cancel = function () {
		   $modalInstance.dismiss('cancel');
		};

		$scope.$on('UpdatePersonControllerLoaded', function(event, data){
			$rootScope.$broadcast('SetPersonToUpdate',  $scope.Person);
		});
});