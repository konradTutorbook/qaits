angular.module('SimpleCsv')
    .controller('UploadPersonCsvController', function($scope, simpleCsvApiUrl, $http, notificationService){

        $scope.UploadComplete = false;

        $scope.Upload = function(){
            var testFile = document.getElementsByName("personCsv")[0].files[0];
            
            var formData = new FormData();
            formData.append("file", testFile);

           $http.post(simpleCsvApiUrl +'Person/UploadCsv', formData, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined }
                    })
                    .success(onPersonCsvUploaded)
                    .error(onPersonCsvUploadFailed);
                };

        var onPersonCsvUploaded = function(results){
            $scope.$emit('SetPersons', results);
            $scope.$emit('SetInvalidResults', results);
            notificationService.success('You have successfully uploaded ' + results.Results.length + ' people.');
            notificationService.error(results.InvalidImportResults.length + ' people have failed to upload.');
            $scope.UploadComplete = true;
        }

        $scope.$on('PersonUpdated', function(){
            $scope.$broadcast('GetPersons', 1);
        })

        var onPersonCsvUploadFailed = function(results){
            notificationService.error(results.Message);
        };

    });