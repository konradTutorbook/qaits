angular.module('SimpleCsv')
	.controller('PersonsController', function($scope, $rootScope, SimpleCsvApiService, notificationService, $modal){

		$scope.PageNumber = 1;

		$scope.Persons ={
			Results: []
		};

		$scope.GetPersons = function(){
			SimpleCsvApiService.GetPersons($scope.PageNumber)
				.success(personsLoaded)
				.error(personsLoadedFailed);
		};

		var personsLoaded = function(results){
			$scope.Persons = results;
			$rootScope.$emit('PersonsLoaded', results);
		};

		var personsLoadedFailed = function(results){

		}

		$scope.EditPerson = function(person){
			var modalInstance = $modal.open({
		      templateUrl: 'Components/UpdatePersonModal.html',
		      controller: 'UpdatePersonModalController',
		      size: 'lg',
		      resolve: {
		        person: function () {
		          return person;
		        }
		      }
		    });
		}

		$scope.DeletePerson = function(id){
			SimpleCsvApiService.DeletePerson(id)
				.success(onPersonDeleted)
				.error(onPersonDeleteFailed);
		}

		var onPersonDeleted = function(results){
			notificationService.success(results.Message);
			$scope.GetPersons();
		}

		var onPersonDeleteFailed = function(results){
			notificationService.success(results.Message);
		}

		$scope.$on('GetPersons', function(event, pageNumber){
			$scope.PageNumber = pageNumber;
			$scope.GetPersons();
		});

		$scope.$on('SetPersons', function(event, results){
			$scope.Persons = results;
		});

		$scope.$emit('PersonsControllerLoaded', {});
	});