angular.module('SimpleCsv')
	.controller('PersonViewController', function($scope){

		$scope.$on('PersonsControllerLoaded', function(event, data){
			$scope.$broadcast('GetPersons', 1);
		});
	});