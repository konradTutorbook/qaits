angular.module('SimpleCsv')
	.controller('InvalidPersonCsvUploadController', function($scope, $modal){

		$scope.UpdatePerson = false;

		$scope.InvalidImportResults = [];

		$scope.$on('SetInvalidResults', function(event, results){
			$scope.InvalidImportResults = results.InvalidImportResults;
		});

		$scope.OnPersonUpdate= function(){
			var modalInstance = $modal.open({
		      templateUrl: 'Components/UpdatePersonModal.html',
		      controller: 'UpdatePersonModalController',
		      size: 'lg',
		      resolve: {
		        person: function () {
		          return {
		          	Id: 0,
		          	FirstName: '',
		          	Surname: '',
		          	Age: 0,
		          	Sex: '',
		          	Mobile: ''
		          };
		        }
		      }
		    });
			
		};

	});