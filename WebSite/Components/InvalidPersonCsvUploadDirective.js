angular.module('SimpleCsv')
	.directive('invalidPersonCsvUpload', function(){
        return {
             restrict: 'E',
                priority: 0,
                templateUrl: 'Components/InvalidPersonCsvUpload.html',
                controller: 'InvalidPersonCsvUploadController'
            }
        });