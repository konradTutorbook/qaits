
angular.module('SimpleCsv')
	.controller('UpdatePersonController', function($scope, $rootScope, SimpleCsvApiService, notificationService){

		$scope.Person = {
			Id: 0,
			FirstName: '',
			Surname: '',
			Age: 0,
			Sex: '',
			Mobile: '',
			IsActive: false
		}

		$scope.Genders = ['Male', 'Female'];

		$scope.UpdatePerson = function(){
			SimpleCsvApiService.UpdatePerson($scope.Person)
				.success(personUpdated)
				.error(personUpdatedFailed);
		}

		var personUpdated = function(results){
			notificationService.success('Person updated succesfully.');
			$scope.$emit('PersonUpdated', {});
		};

		var personUpdatedFailed = function(results){
			notificationService.error(results.Message);
		};

		$scope.$on('SetPersonToUpdate', function(event, person){
			$scope.Person = person;
		});

		$scope.$emit('UpdatePersonControllerLoaded', {});
	});