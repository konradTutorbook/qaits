﻿namespace Qaits
{
    public interface IValidateEntity<in TEntity>  where TEntity : IEntity
    {
        void Validate(TEntity command);
    }
}