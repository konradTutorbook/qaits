﻿using System;
using System.Collections.Generic;
using Qaits.Contracts;
using Qaits.Exceptions;

namespace Qaits.ValueObjects
{
    public struct Sex
    {
        private string gender;
        
        public Sex(string gender)
        {
            this.gender = ParseGender(gender);
        }

        public override string ToString()
        {
            return gender;
        }

        public static ValidationResult[] Validate(string gender)
        {
            var validationResults = new List<ValidationResult>();

            if (String.IsNullOrWhiteSpace(gender))
                validationResults.Add(new ValidationResult("Sex", "Sex cannot be empty."));

            if (!IsValidGender(gender))
                validationResults.Add(InvalidValidationResult());

            return validationResults.ToArray();
        }

        public static bool IsValidGender(string gender)
        {
            return gender.Trim().StartsWith("M", StringComparison.OrdinalIgnoreCase) || gender.Trim().StartsWith("F", StringComparison.OrdinalIgnoreCase);
        }

        public static string ParseGender(string gender)
        {
            if (gender.Trim().StartsWith("M", StringComparison.OrdinalIgnoreCase))
            {
                return "Male";
            }
            if (gender.Trim().StartsWith("F", StringComparison.OrdinalIgnoreCase))
            {
                return "Female";
            }
            throw new ValidationException(new[]
                {
                    InvalidValidationResult() 
                });
        }

        private static ValidationResult InvalidValidationResult()
        {
            return new ValidationResult("Sex", "Please provider a valid sex. Male, Female, M, F");
        }
    }
}
