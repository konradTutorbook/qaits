﻿using System;
using Qaits.Contracts;

namespace Qaits.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationResult[] ValidationResult { get; private set; }

        public ValidationException(ValidationResult[] validationResult)
        {
            ValidationResult = validationResult;
        }
    }
}
