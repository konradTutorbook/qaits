﻿using System;
using System.Collections.Generic;
using System.Linq;
using Qaits.Contracts;
using Qaits.Exceptions;
using Qaits.Persistence;
using Qaits.ValueObjects;

namespace Qaits.Api.Validators
{
    public class EntityValidator : IValidateEntity<Person>
    {
        public void Validate(Person entity)
        {
            var validationResult = new List<ValidationResult>();

            if(entity.Id == 0)
                validationResult.Add(new ValidationResult("Id", "Id cannot be 0"));

            if (String.IsNullOrWhiteSpace(entity.FirstName))
                validationResult.Add(new ValidationResult("FirstName", "Firstname cannot be empty."));

            if (String.IsNullOrWhiteSpace(entity.Surname))
                validationResult.Add(new ValidationResult("Surname", "Surname cannot be empty."));
            
            if (String.IsNullOrWhiteSpace(entity.Sex))
                validationResult.Add(new ValidationResult("Sex", "Sex cannot be empty."));

            if (entity.Age == 0)
                validationResult.Add(new ValidationResult("Age", "You cannot be zero years old."));

            if (String.IsNullOrWhiteSpace(entity.Mobile))
                validationResult.Add(new ValidationResult("Mobile", "Your mobile number cannot be empty."));

            if(!PhoneNumber.IsMobile(entity.Mobile))
                validationResult.Add(new ValidationResult("Mobile", String.Format("{0} is not a valid mobile number.", entity.Mobile)));

            validationResult.AddRange(Sex.Validate(entity.Sex));

            if (validationResult.Any())
            {
                throw new ValidationException(validationResult.ToArray());
            }
        }
    }
}