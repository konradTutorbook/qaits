﻿using System.Data.Entity;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Elmah.Contrib.WebApi;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using Qaits.Api;
using Qaits.Api.Validators;
using Qaits.Persistence;

[assembly: OwinStartup(typeof(Startup))]
namespace Qaits.Api
{
    public static class RouteNames
    {
        public static string DefaultApi = "defaultApi";
    }

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);

            var container = new ContainerBuilder();
            container.RegisterApiControllers(Assembly.GetExecutingAssembly()).InstancePerRequest();
            container.RegisterType<EntityValidator>().InstancePerRequest();
            var builtContainer = container.Build();

            app.UseWebApi(ConfigureWebApi(builtContainer));
            
            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(builtContainer);

            IDatabaseInitializer<DbContext> quaitsContext = new CreateDatabaseIfNotExists<DbContext>();
            Database.SetInitializer(quaitsContext);
        }

        private static HttpConfiguration ConfigureWebApi(IContainer container)
        {
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            config.Routes.MapHttpRoute(
                name: RouteNames.DefaultApi,
                routeTemplate: "api/{controller}/{id}",
                defaults: new
                {
                    id = RouteParameter.Optional
                });
            config.Filters.Add(new ElmahHandleErrorApiAttribute());
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            return config;
        }
    }
}