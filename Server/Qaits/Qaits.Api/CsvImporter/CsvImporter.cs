﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Qaits.Exceptions;

namespace Qaits.Api.CsvImporter
{
    public abstract class CsvImporter<TResult>
    {
        private readonly string filePathForDrop = ConfigurationManager.AppSettings["tempFilePath"];

        public abstract char Delimiter { get; }
        
        public async Task<ImportCsvResult<TResult>> ImportCsv(HttpContent httpContent)
        {
            var provider = GetMultipartProvider();
            var result = await httpContent.ReadAsMultipartAsync(provider);
            
            var uploadedFileInfo = new FileInfo(result.FileData.First().LocalFileName);

            var lines = File.ReadAllLines(uploadedFileInfo.FullName);

            var results = new List<TResult>();
            var invalidResults = new List<InvalidImportResult>();

            bool firstLine = true;

            foreach (var line in lines)
            {
                if (firstLine)
                {
                    firstLine = false;
                    continue;
                }
                
                var csvDelimited = line.Split(Delimiter);

                try
                {
                    var convertedResult = Convert(csvDelimited);
                    results.Add(convertedResult);
                }
                catch (ValidationException validationException)
                {
                    var invalidResult = WhenInvalid(line, csvDelimited, validationException);
                    invalidResults.Add(invalidResult);
                }
                catch (IndexOutOfRangeException)
                {
                    var invalidResult = WhenInvalid(line, csvDelimited);
                    invalidResults.Add(invalidResult);
                }
            }

            return new ImportCsvResult<TResult>(results.ToArray(), invalidResults.ToArray());
        }
      
        public abstract TResult Convert(string[] csvDelimited);
        public abstract InvalidImportResult WhenInvalid(string line, string[] exportResults);
        public abstract InvalidImportResult WhenInvalid(string line, string[] exportResults, ValidationException validationException);

        private MultipartFormDataStreamProvider GetMultipartProvider()
        {
            var root = filePathForDrop;
            Directory.CreateDirectory(root);
            return new MultipartFormDataStreamProvider(root);
        }
    }
}