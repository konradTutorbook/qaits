﻿using Qaits.Contracts;

namespace Qaits.Api.CsvImporter
{
    public class InvalidImportResult
    {
        public string Line { get; protected set; }
        public string[] ErrorMessages { get; protected set; }
        public ValidationResult[] ValidationResults { get; protected set; }

        public InvalidImportResult(string line, string[] errorMessages, ValidationResult[] validationResults)
        {
            Line = line;
            ErrorMessages = errorMessages;
            ValidationResults = validationResults;
        }

        public InvalidImportResult(string line, string[] errorMessages)
        {
            Line = line;
            ErrorMessages = errorMessages;
            ValidationResults = new ValidationResult[0];
        }
    }
}