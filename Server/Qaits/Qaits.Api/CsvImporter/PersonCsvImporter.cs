﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Qaits.Api.Validators;
using Qaits.Contracts;
using Qaits.Exceptions;
using Qaits.Persistence;
using Qaits.ValueObjects;

namespace Qaits.Api.CsvImporter
{
    public class PersonCsvImporter : CsvImporter<Person>
    {
        private readonly EntityValidator entityValidator;
        public override char Delimiter{get { return ','; }}

        public PersonCsvImporter(EntityValidator entityValidator)
        {
            this.entityValidator = entityValidator;
        }

        public override Person Convert(string[] csvDelimited)
        {
            var validationResults = new List<ValidationResult>();
            
            int id = 0;
            int age = 0;
            bool isActive = false;
          
            if (!int.TryParse(csvDelimited[0], out id))
            {
                validationResults.Add(new ValidationResult("Id", "Please enter a valid Id"));
            }
            if (!int.TryParse(csvDelimited[3], out age))
            {
                validationResults.Add(new ValidationResult("Age", "Please enter a age"));
            }
            if (!bool.TryParse(csvDelimited[6], out isActive))
            {
                validationResults.Add(new ValidationResult("IsActive", "Please enter a true or false"));
            }
            validationResults.AddRange(Sex.Validate(csvDelimited[4]));

            if (validationResults.Count != 0)
            {
                throw new ValidationException(validationResults.ToArray());
            }

            var person = new Person
            {
                Id = id,
                FirstName = csvDelimited[1],
                Surname = csvDelimited[2],
                Age = age,
                Sex = new Sex(csvDelimited[4]).ToString(),
                Mobile = csvDelimited[5],
                IsActive = isActive
            };

            entityValidator.Validate(person);

            return person;
        }

        public override InvalidImportResult WhenInvalid(string line, string[] exportResults)
        {
            var errorMessages = GetErrorMessages(exportResults);
            
            return new InvalidImportResult(line, errorMessages.ToArray());
        }

        public override InvalidImportResult WhenInvalid(string line, string[] exportResults, ValidationException validationException)
        {
            return new InvalidImportResult(line, new[] { "The details of the person you have entered are invalid." }, validationException.ValidationResult);
        }
        
        private List<string> GetErrorMessages(string[] exportResults)
        {
            var errorMessages = new List<string>();

            if (exportResults.Length > 8)
            {
                errorMessages.Add(String.Format("To many '{0}' where found.", Delimiter));
            }
            if (exportResults.Length < 7)
            {
                errorMessages.Add(String.Format("There where not enough '{0}' found.", Delimiter));
            }
            return errorMessages;
        }
    }
}