﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Qaits.Api.Controllers
{
    public class TestController : ApiController
    {
        [Route("api/Test")]
        public HttpResponseMessage GetTest()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "Hello Api");
        }
    }
}