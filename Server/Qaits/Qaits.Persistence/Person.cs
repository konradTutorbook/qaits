﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Qaits.Persistence
{
    [Table("Person")]
    public class Person : IEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public virtual int Id { get; set; }
        
        [Required, StringLength(50)]
        public virtual string FirstName { get; set; }

        [Required, StringLength(50)]
        public virtual string Surname { get; set; }

        [Required, StringLength(10)]
        public virtual string Sex { get; set; }

        [Required]
        public virtual int Age { get; set; }

        [Required, StringLength(13)]
        public virtual string Mobile { get; set; }

        [Required]
        public virtual bool IsActive { get; set; }
    }
}
